<?php
/**
 * Created by PhpStorm.
 * User: Harry van der Valk @ HVSoftware
 * Date: 18-5-14
 * Time: 16:36
 */

class UserController extends \BaseController {

    public function settings() {
        $user = Auth::user();
        return View::make('user.settings', compact('user'));
    }
    public function requestpwd() {
        $lang =Input::get('language');

        App::setLocale($lang);
        return View::make('auth.requestpwd');
    }
    public function requestpwdhandler() {
        echo "paswoord werd gereset - check uw email";
        return View::make('auth.requestconf');
    }
} 