<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to($resource) }}">{{ ucfirst($resource) }} Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to($resource) }}">View All {{ ucfirst($resource) }}</a></li>
        <li><a href="{{ URL::to($resource . '/create') }}">Create {{ ucfirst($resource) }}</a>
    </ul>
</nav>
 