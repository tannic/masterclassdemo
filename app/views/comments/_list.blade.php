<div class="jumbotron text-left">
    <h2>Comments</h2>
    <ul>
        @foreach($list as $item)
            <li><a href="{{ URL::route( 'comments.show', array('id' => $item->id)) }}">{{ $item->content }}</a> by {{ $item->user()->name }}</li>
        @endforeach
    </ul>
</div>