@extends('layouts.bootstrap')

@section('title')
REquest password
@stop

@section('content')

<h1>{{Lang::get('reminders.request')}}</h1>


<form action="{{URL::route('users.requesthandler')}}" method="post" class="form-signin" role="form">
    <input type="email" class="form-control" placeholder="Email address" required autofocus>
    <button class="btn btn-lg btn-primary btn-block" type="submit">{{Lang::get('reminders.submit')}}</button>
    <a href="?language=nl">NL</a>
    <a href="?language=en">EN</a>
</form>

@stop

