@extends('layouts.bootstrap')

@section('title')
Login
@stop

@section('content')

<h1>Login</h1>


<form class="form-signin" role="form" action="{{ URL::route('login') }}" method="post">
    <input type="email" class="form-control" placeholder="Email address" required autofocus>
    <input type="password" class="form-control" placeholder="Password" required>
    <label class="checkbox">
        <input type="checkbox" value="remember-me"> Remember me
    </label>
    <a href="{{URL::route('users.request')}}">paswoord vergeten?</a>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
</form>

@stop