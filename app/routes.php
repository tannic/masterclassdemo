<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;

//use Studyx\Pages\Page;

// App::make('page')->disable();

require_once "routes_cache.php";
require_once "routes_sessions.php";
require_once "routes_admin.php";

Route::get(
    '/page/test',
    function () use ($app) {
        return $app['page']->testMe();
    }
);

Route::get(
    '/page/report',
    function () use ($app) {
        //return $app['page']->generateReport();
        return Page::generateReport();
    }
);

Route::get(
    '/page/toggle',
    function () use ($app) {
        //$app['page']->disable();
        Page::toggle();

        return "Page toggled";
    }
);
Route::get(
    '/page/state/{active}',
    function ($active) use ($app) {
        if ($active) {
            //$app['page']->disable();
            Page::disable();

            return "Page info disabled";
        } else {
            //$app['page']->enable();
            Page::enable();

            return "Page info enabled";
        }
    }
);

Route::get(
    '/test',
    array(
        function () {
            return View::make('test');
        }
    )
);

Route::resource('categories', 'CategoryController');
Route::resource('comments', 'CommentsController');
Route::resource('posts', 'PostsController');
Route::resource('tags', 'TagsController');

Route::get(
    'user/login',
    array(
        'as' => 'login',
        function () {
            return View::make('auth.login');
        }
    )
);

Route::post(
    'user/login',
    array(
        'as' => 'login',
        function () {
            if (Auth::loginUsingId(1)) {
                return View::make('auth.login_success');
            } else {
                return View::make('auth.login_failed');
            }
        }
    )
);

Route::get(
    'user/logout',
    array(
        'as' => 'logout',
        function () {
            Auth::logout();

            return View::make('auth.logout');
        }
    )
);

Route::group(
    array('before' => 'auth'),
    function () {
        Route::get(
            '/auth/settings',
            array(
                'as' => 'user.settings',
                'uses' => 'UserController@settings'
            )
        );
    }
);


Route::get(
    'lang/{lang}',
    function ($lang) {
        $languages = Config::get('app.languages');

        if (in_array($lang, $languages)) {
            Session::put('lang', $lang);
        }

        return Redirect::back();
    }
);


Route::get(
    '/about',
    array(
        'as' => 'about',
        function () {
            return View::make('pages.about');
        }
    )
);


Route::get(
    '/contact',
    array(
        'as' => 'contact',
        function () {
            return View::make('pages.contact');
        }
    )
);

Route::get(
    '/',
    array(
        'as' => 'home',
        'uses' => 'HomeController@frontPage'
    )
);

Route::get(
    '/show',
    function () {
        return View::make('hello');
    }
);

Route::get('/showme', 'HomeController@showWelcome');

//Route::get(
//    'pages',
//    function () {
//
//    }
//);

Route::get(
    'user/{name?}',
    function ($name = 'John') {
        return $name;
    }
);

//Route::get(
//    '{lang?}/{slug?}',
//    function ($lang = 'nl', $slug = 'default') {
////  dezesite.nl/nl/about
////  dezesite.nl/fr/apro
//        return "Get the $lang page with $slug";
//    }
//);

//App::missing(function($exception)
//    {
//        return Response::view('error', array(), 404);
//    });

Route::post('category', array('as' => 'category.create', 'uses' => 'CategoryController@store'));

Route::get(
    '/test',
    array(
        'as' => 'test',
        function () {
            $user = Auth::user();
            $posts = $user->getPosts();
            echo count($posts);
            var_dump($posts);
            exit;
            $comments = Post::find(1)->comments;
            var_dump($comments);

            //var_dump(Auth::user());
            return View::make('test');
        }
    )
);

Route::resource('posts', 'PostsController');
Route::resource('postEvents', 'PostEventsController');
Route::resource('comments', 'CommentsController');
Route::resource('categories', 'CategoriesController');
Route::resource('tags', 'TagsController');
Route::get('users/request',array('as'=> 'users.request','uses'=>'UserController@requestpwd'));
Route::post('users/requesthandler',array('as'=> 'users.requesthandler','uses'=>'UserController@requestpwdhandler'));
