## Master Class Blog Systeem

A system to create your own blogs

## Official Documentation

Documentation are stored in Doc.

### Contributing

0.1 First release
 * https://github.com/JeffreyWay/Laravel-4-Generators (via composer)
 * http://bundles.laravel.com/bundle/bob (via bundles - oude methode)

0.2 After install Laravel-4-Generators

0.3 With Generators - without Models and Views and all other stuff

0.4 With Generators and Implemented Tag
 * Try to switch from tag by: git tag <tag>

0.5 App Bootstrap ready maken
 * visit http://getbootstrap.com/
 * Install bootstrap by Componser
 * Copy files to local - in not done by copy_bootstrap
 * get Basic template site from http://getbootstrap.com/getting-started/
 * Save under bootstrap_basic.html
 * Make copy to bootstrap.blade.php (master layout)
 * Extract header to header.blade.php and include in master layout using @include('layouts.header')
 * Extract footer to footer.blade.php and include in master layout using @include('layouts.footer')
 * Checkout the examples
 * layouts.bootstrap

0.6 Using the session
 * Session::put one value
 * Session::push an array
 * Session::get a value
 * Session::all get all data
 * Session::forget forgets a key
 * Session::flush flushes all keys
 * Illuminate\Session\CommandsServiceProvider
 * Illuminate\Session\SessionServiceProvider
 * Illuminate\Support\Facades\Session

0.7 Cache and moved routes to separate folders
 * Illuminate\Support\Facades\Cache
 * Illuminate\Cache\CacheServiceProvider

0.8 Package development
 * Make it small
 * php artisan workbench vendor/studyx --resources
 * Available in workbench folder
 * Regiser your privider in app/config/app.php
 * The Package Folder Structure

0.9 Authenticatie
 * Routing
 * Auth Controller
 * Packages
  * https://github.com/cartalyst/sentry
  * https://github.com/Zizaco/confide
  * https://github.com/Toddish/Verify-L4
 * Illuminate\Auth\AuthServiceProvider
 * Illuminate\Support\Facades\Auth

0.10 Helpers
 * Helpers binnen Laravel
 * See within app.php
  * providers
  * aliases

0.11 Commands
 * Zie Cache Clear Commands

0.12 Facades

0.13 Faker
 * Composer: "fzaninotto/faker": "v1.3.0"
 * use Faker\Factory as Faker;
 * https://github.com/fzaninotto/Faker

### Laravel

This application is based on Laravel
